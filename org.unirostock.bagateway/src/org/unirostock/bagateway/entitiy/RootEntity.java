/*
 * File: RootEntity.java
 * Project: org.unirostock.bagateway
 * @author Robert Balla
 */
package org.unirostock.bagateway.entitiy;

import java.util.Hashtable;
import java.util.LinkedList;

/**
 * The Class RootEntity.
 * 
 * Represents the device itself. It's the level 0 entity.
 */
public class RootEntity extends GatewayInternalEntity
{
	
	/** The origin protocol.
	 * 
	 * protocol where the device belongs to
	 * */
	private String originProtocol;
	
	/** The identifier table. 
	 * 
	 * contains the identifiers other gates refers to the device
	 * */
	private Hashtable<String, Object> identifierTable;
	
	/**
	 * Instantiates a new root entity.
	 *
	 * @param protocol the protocol
	 * @param name the name
	 * @param id the id
	 */
	public RootEntity (String protocol, String name, Object id)
	{
		super(name, id, 0, new LinkedList<Object>());
		
		originProtocol = protocol;
		identifierTable = null;
	}
	
	/**
	 * Gets the origin protocol.
	 *
	 * @return the origin protocol
	 */
	public String getOriginProtocol()
	{
		return this.originProtocol;
	}
	
	/**
	 * Adds the identifier.
	 *
	 * adds the identifier to the identifierTable with the protocol as key
	 * 
	 * @param protocol the protocol
	 * @param id the id
	 */
	public void addIdentifier(String protocol, Object id)
	{
		/*
		 * check if there already is an identifierTable
		 */
		if (identifierTable == null)
		{
			this.identifierTable = new Hashtable<String, Object>();
		}
		
		/*
		 * adds the identifier to the identifierTable with the protocol as key
		 */
		identifierTable.put(protocol, id);
	}
	
	/**
	 * Gets the identifier.
	 *
	 * @param protocol the protocol
	 * @return the identifier
	 */
	public Object getIdentifier(String protocol)
	{
		/*
		 * if there is no identifierTable or it doesn't contains the protocol return null 
		 */
		if (identifierTable == null || !identifierTable.containsKey(protocol))
		{
			return null;
		}
		
		/*
		 * return the identifier belongs to the protocol
		 */
		return identifierTable.get(protocol);
	}
}
