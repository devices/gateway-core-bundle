/*
 * File: Entitiy.java
 * Project: org.unirostock.bagateway
 * @author Robert Balla
 */
package org.unirostock.bagateway.entitiy;

import java.util.LinkedList;

/**
 * The Class GatewayInternalEntity.
 * 
 * The base-structure for representing devices within the gateway
 */
public class GatewayInternalEntity
{
	
	/** The level. 
	 * 
	 * The level in the hierarchy
	 * */
	private Integer level;
	
	/** The name. 
	 * 
	 * The name of the entity
	 * */
	private String name;
	
	/** The origin identifier. 
	 * 
	 * The origin identifier of the entity to address it in the appropriate gate
	 * */
	private Object originIdentifier;
	
	/** The path. 
	 * 
	 * The path contains all entity-IDs of all previous entities in the hierarchy
	 * */
	private LinkedList<Object> path;
	
	/** The sub entities. 
	 * 
	 *  A list of all subEntities the entity has
	 * */
	private LinkedList<SubEntity> subEntities;
	
	
	
	/**
	 * Instantiates a new gateway internal entity.
	 *
	 * @param name the name
	 * @param id the id
	 * @param level the level
	 * @param path the path
	 */
	public GatewayInternalEntity (String name, Object id, Integer level, LinkedList<Object> path)
	{
		this.level = level;
		this.name = name;
		this.originIdentifier = id;
		this.path = new LinkedList<Object>();
		this.path.addAll(path);
		this.path.add(id);
		this.subEntities = null;
	}
	
	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public Integer getLevel()
	{
		return this.level;
	}
	
	/**
	 * Gets the origin name.
	 *
	 * @return the origin name
	 */
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * Gets the origin identifier.
	 *
	 * @return the origin identifier
	 */
	public Object getOriginIdentifier()
	{
		return this.originIdentifier;
	}
	
	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public LinkedList<Object> getPath()
	{
		return this.path;
	}
	
	/**
	 * Adds the sub entity.
	 *
	 * adding a subEntity to the list
	 * 
	 * @param name the name
	 * @param id the id
	 */
	public void addSubEntity(String name, Object id)
	{
		/*
		 * check if there is a list of subEntities
		 */
		if (subEntities == null)
			subEntities = new LinkedList<SubEntity>();
		
		/*
		 * generate an new subEntity
		 * and add a new subEntity to the list
		 */
		subEntities.add(new SubEntity(name, id, level+1, this.path));
	}
	
	/**
	 * Adds the sub entity.
	 *
	 * adding a subEntity to the list
	 *
	 * @param ent the ent
	 */
	public void addSubEntity(SubEntity ent)
	{
		/*
		 * check if there is a list of subEntities
		 */
		if (subEntities == null)
			subEntities = new LinkedList<SubEntity>();
		
		/*
		 * add a new subEntity to the list
		 */
		subEntities.add(ent);
	}
	
	/**
	 * Gets the sub entity.
	 * 
	 * get a single subEntity by the originId
	 *
	 * @param id the id
	 * @return the sub entity
	 */
	public SubEntity getSubEntity(Object id)
	{
		/*
		 * check if there is a list of subEntities
		 */
		if (subEntities == null)
			return null;
		
		
		/*
		 * generate the targetEntity with the originId
		 */
		GatewayInternalEntity targetEnt = new GatewayInternalEntity(null, id, null, new LinkedList<Object>());
		
		/*
		 * if the targetEntity is an subEntity of this, return it
		 */
		if (subEntities.contains(targetEnt))
			return subEntities.get(subEntities.indexOf(targetEnt));
		
		/*
		 * search in all subEntities for the targetEntity
		 */
		SubEntity result;
		for (SubEntity subEnt : subEntities)
		{
			result = subEnt.getSubEntity(id);
			if (result != null)
				return result;
		}
		
		/*
		 * targetEntity not found
		 */
		return null;
		
	}
	
	/**
	 * Gets the all sub entities.
	 * 
	 * get the list of subEntities
	 *
	 * @return the all sub entities
	 */
	public LinkedList<SubEntity> getAllSubEntities()
	{
		return subEntities;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (Object o)
	{
		boolean retVal = false;
		
		if (o instanceof GatewayInternalEntity)
		{
			GatewayInternalEntity obj = (GatewayInternalEntity) o;
			
			if (this.originIdentifier.equals(obj.originIdentifier))
			{
				retVal = true;
			}
		}
		
		return retVal;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode ()
	{
		return this.getOriginIdentifier().hashCode();
	}
}
