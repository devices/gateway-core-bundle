/*
 * File: SubEntity.java
 * Project: org.unirostock.bagateway
 * @author Robert Balla
 */
package org.unirostock.bagateway.entitiy;

import java.util.LinkedList;

import org.unirostock.bagateway.enums.GWdatatype;
import org.unirostock.bagateway.enums.GWdirection;
import org.unirostock.bagateway.enums.GWgroup;

/**
 * The Class SubEntity.
 * 
 * represents all inner structures of the device
 */
public class SubEntity extends GatewayInternalEntity
{	
	/** The item. 
	 * 
	 * The item contains the info about the item of the value the entity represents
	 * */
	private GWgroup group;
	
	private Integer count;
	
	private GWdirection direction;
	
	private LinkedList<ParameterEntity> parameter;
	
	/**
	 * Instantiates a new sub entity.
	 *
	 * @param name the name
	 * @param id the id
	 * @param lvl the lvl
	 */
	public SubEntity(String name, Object id, Integer lvl, LinkedList<Object> path)
	{
		super(name, id, lvl, path);
		
		group = null;
		direction = null;
		count = -1;
	}
	
	/**
	 * Gets the item.
	 *
	 * @return the item
	 */
	public GWgroup getGroup()
	{
		return this.group;
	}
	
	/**
	 * Sets the item.
	 *
	 * @param item the new item
	 */
	public void setGroup(GWgroup g)
	{
		this.group = g;
	}
	
	public Integer getCount()
	{
		return this.count;
	}
	
	public void setCount(Integer c)
	{
		this.count = c;
	}
	
	public GWdirection getDirection()
	{
		return this.direction;
	}
	
	public void setDirection(GWdirection d)
	{
		this.direction = d;
	}
	
	public void addParameter(String name, Object id)
	{
		/*
		 * check if there is a list of subEntities
		 */
		if (parameter == null)
			parameter = new LinkedList<ParameterEntity>();
		
		/*
		 * generate an new subEntity
		 * and add a new subEntity to the list
		 */
		parameter.add(new ParameterEntity(name, id, this.getLevel()+1, this.getPath()));
	}

	public void addParameter(ParameterEntity prop)
	{
		/*
		 * check if there is a list of subEntities
		 */
		if (parameter == null)
			parameter = new LinkedList<ParameterEntity>();
		
		/*
		 * add a new subEntity to the list
		 */
		parameter.add(prop);
	}
	
	public ParameterEntity getParameter(Object id)
	{
		/*
		 * check if there is a list of subEntities
		 */
		if (parameter == null)
			return null;
		
		
		/*
		 * generate the targetProperty with the originId
		 */
		ParameterEntity targetProp = new ParameterEntity(null, id, null, new LinkedList<Object>());
		
		/*
		 * if the targetProperty is an property of this, return it
		 */
		if (parameter.contains(targetProp))
			return parameter.get(parameter.indexOf(targetProp));
		
		/*
		 * search in all subEntities for the targetProperty
		 */
		ParameterEntity result;
		LinkedList<SubEntity> subEntities = this.getAllSubEntities();
		for (SubEntity subEnt : subEntities)
		{
			result = subEnt.getParameter(targetProp);
			if (result != null)
				return result;
		}
		
		/*
		 * targetProperty not found
		 */
		return null;
		
	}
	
	public LinkedList<ParameterEntity> getAllParameter()
	{
		return this.parameter;
	}
}
