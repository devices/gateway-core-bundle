package org.unirostock.bagateway.entitiy;

import java.util.LinkedList;

import org.unirostock.bagateway.enums.GWdatatype;
import org.unirostock.bagateway.enums.GWparameter;

public class ParameterEntity extends GatewayInternalEntity
{
	/** The attributes. 
	 * 
	 * The attributes contains the info whether the entity represents a value
	 * and if it's readable, writeable and/or orderable 
	 * */
	private Attribute attributes;
	
	/** The datatype. 
	 * 
	 * The datatype contains the info about the datatype of the value the entity represents
	 * */
	private GWdatatype datatype;
	
	private GWparameter parameterName;
	
	/** The last value. 
	 * 
	 * contains the last noted value
	 * */
	private String lastValue;
	
	public ParameterEntity(String name, Object id, Integer lvl, LinkedList<Object> path)
	{
		super(name, id, lvl, path);
		
		this.datatype = null;
		this.parameterName = null;
		this.lastValue = null;
		this.attributes = new Attribute();
	}
	
	/**
	 * Gets the datatype.
	 *
	 * @return the datatype
	 */
	public GWdatatype getDatatype()
	{
		return this.datatype;
	}
	
	/**
	 * Sets the datatype.
	 *
	 * @param type the new datatype
	 */
	public void setDatatype(GWdatatype type)
	{
		this.datatype = type;
	}
	
	public GWparameter getParameterName()
	{
		return this.parameterName;
	}
	
	public void setParameterName(GWparameter name)
	{
		this.parameterName = name;
	}
	
	/**
	 * Checks if is readable.
	 *
	 * @return the boolean
	 */
	public Boolean isReadable()
	{
		return attributes.readable;
	}
	
	/**
	 * Checks if is writeable.
	 *
	 * @return the boolean
	 */
	public Boolean isWriteable()
	{
		return attributes.writeable;
	}
	
	/**
	 * Checks if is orderable.
	 *
	 * @return the boolean
	 */
	public Boolean isSubscribable()
	{
		return attributes.subscribable;
	}
	
	/**
	 * Sets the readable.
	 *
	 * @param bool the new readable
	 */
	public void setReadable(Boolean bool)
	{
		attributes.readable = bool;
	}
	
	/**
	 * Sets the writeable.
	 *
	 * @param bool the new writeable
	 */
	public void setWriteable(Boolean bool)
	{
		attributes.writeable = bool;
	}
	
	/**
	 * Sets the orderable.
	 *
	 * @param bool the new orderable
	 */
	public void setSubscribable(Boolean bool)
	{
		attributes.subscribable = bool;
	}
	
	public String getLastValue()
	{
		return this.lastValue;
	}
	
	public void setLastValue(String val)
	{
		this.lastValue = val;
	}
	
	@Override
	public void addSubEntity(String name, Object id)
	{
		try
		{
			throw new Exception("PropertyEntity has no SubEntities!");
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void addSubEntity(SubEntity ent)
	{
		try
		{
			throw new Exception("PropertyEntity has no SubEntities!");
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	@Override
	public SubEntity getSubEntity(Object id)
	{
		try
		{
			throw new Exception("PropertyEntity has no SubEntities!");
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	public LinkedList<SubEntity> getAllSubEntities()
	{
		try
		{
			throw new Exception("PropertyEntity has no SubEntities!");
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
		
	
	/**
	 * The Class Attribute.
	 */
	private class Attribute
	{
		
		/** The readable. */
		public Boolean readable;
		
		/** The writeable. */
		public Boolean writeable;
		
		/** The orderable. */
		public Boolean subscribable;
		
		/**
		 * Instantiates a new attribute.
		 */
		public Attribute()
		{
			readable = false;
			writeable = false;
			subscribable = false;
		}
	}
}
