/*
 * File: GatewayInternalProperty.java
 * Project: org.unirostock.bagateway
 * @author Robert Balla
 */
package org.unirostock.bagateway.enums;


public enum GWparameter
{
	INFO							{public String toString(){return "info";}},
	INFO_ACTIVE					{public String toString(){return "infoIfActive";}},
	INFO_INACTIVE				{public String toString(){return "infoIfInactive";}},
	NAME							{public String toString(){return "name";}},
	NOTIFICATION_DELAY		{public String toString(){return "delayForNotification";}},
	NOTIFICATION_INCREMENT	{public String toString(){return "incrementForNotification";}},
	NOTIFICATION_LIMIT_MAX	{public String toString(){return "upperLimitForNotification";}},
	NOTIFICATION_LIMIT_MIN	{public String toString(){return "lowerLimitForNotification";}},
	NOTIFICATION_LIMITS_ON	{public String toString(){return "limitNotificationActive";}},
	PRESENT_VALUE				{public String toString(){return "presentValue";}},
	PRESVAL_UPDATEINTERVAL	{public String toString(){return "updateitervalOfPresentValue";}},
	PRESVAL_LIMIT_MAX			{public String toString(){return "upperBoundOfPresentValue";}},
	PRESVAL_LIMIT_MIN			{public String toString(){return "lowerBoundOfPresentValue";}},
	PRESVAL_STATES_NUM		{public String toString(){return "numberOfStatesOfPresentValue";}},
	PRESVAL_STATES_INFO		{public String toString(){return "statesOfPresentValue";}},
	PRESVAL_POLARITY			{public String toString(){return "polarityOfPresentValue";}},
	PRESVAL_RESOLUTION		{public String toString(){return "resolutionOfPresentValue";}},
	PRESVAL_UNIT				{public String toString(){return "unitOfPresentValue";}},
	STATUS						{public String toString(){return "status";}};
}
