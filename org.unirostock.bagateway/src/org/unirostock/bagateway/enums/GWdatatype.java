/*
 * File: GatewayInternalDatatype.java
 * Project: org.unirostock.bagateway
 * @author Robert Balla
 */
package org.unirostock.bagateway.enums;

public enum GWdatatype
{
	Boolean, 
	Byte,
	CharacterString,
	Double,
	Float,
	Integer,
	Unit,
	UnsignedInteger,
}
