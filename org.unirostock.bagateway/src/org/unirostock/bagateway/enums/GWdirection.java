package org.unirostock.bagateway.enums;

public enum GWdirection
{
	INPUT {public String toString(){return "Input";}},
	OUTPUT {public String toString(){return "Output";}},
	VALUE {public String toString(){return "Value";}}
}
