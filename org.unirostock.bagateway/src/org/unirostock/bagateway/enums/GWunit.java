package org.unirostock.bagateway.enums;

public enum GWunit
{
	//Others
	Percent		{public String toString(){return "Percent";}},
	
	//Temperature
	Celsius		{public String toString(){return "Celsius";}},
	Fahrenheit	{public String toString(){return "Fahrenheit";}},
	Kelvin		{public String toString(){return "Kelvin";}};
	
	
}
