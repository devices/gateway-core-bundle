/*
 * File: GatewayInternalItem.java
 * Project: org.unirostock.bagateway
 * @author Robert Balla
 */
package org.unirostock.bagateway.enums;

public enum GWgroup
{
	ACCELERATION			{public String toString(){return "Acceleration";}},
	AREA						{public String toString(){return "Area";}},
	
	CAPACITANCE				{public String toString(){return "Capacitance";}},
	CONDUCTANCE				{public String toString(){return "Conductance";}},
	CURRENT					{public String toString(){return "Current";}},
	
	ENERGY					{public String toString(){return "Energy";}},
	
	FORCE						{public String toString(){return "Force";}},
	FREQUENCY				{public String toString(){return "Frequency";}},
	
	HUMIDITY					{public String toString(){return "Humidity";}},
	
	ILLUMINATION			{public String toString(){return "Illumination";}},
	INDUCTANCE				{public String toString(){return "Inductance";}},
	
	LENGTH					{public String toString(){return "Length";}},
	LUMINOUS_FLUX			{public String toString(){return "LuminousFlux";}},
	LUMINOUS_INTENSITY	{public String toString(){return "LuminousIntensity";}},
	
	MASS						{public String toString(){return "Mass";}},
	
	POWER						{public String toString(){return "Power";}},
	PRESSURE					{public String toString(){return "Pressure";}},
	
	RESISTANCE				{public String toString(){return "Resistance";}},
	
	TEMPERATURE				{public String toString(){return "Temperature";}},
	TIME						{public String toString(){return "Time";}},
	
	VELOCITY					{public String toString(){return "Velocity";}},
	VOLTAGE					{public String toString(){return "Volatage";}},
	VOLUME					{public String toString(){return "Volume";}},
	
	BINARY					{public String toString(){return "Binary";}},
	MULTISTATE				{public String toString(){return "Multistate";}}
}
