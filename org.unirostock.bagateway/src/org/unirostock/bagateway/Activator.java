/*
 * File: Activator.java
 * Project: org.unirostock.bagateway
 * @author Robert Balla
 */
package org.unirostock.bagateway;

import java.util.logging.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.util.tracker.ServiceTracker;
import org.unirostock.bagateway.interfaces.GateInterface;
import org.unirostock.bagateway.interfaces.GatewayInterface;

/**
 * The Class Activator.
 */
public class Activator implements BundleActivator
{
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(GatewayImpl.class.getName() + "(Activator)");
	
	/** The context. */
	private static BundleContext context;
	
	/** The gateway reg. */
	private ServiceRegistration<?> gatewayReg;
	
	/** The gateway service. */
	private GatewayImpl gatewayService;
	
	/** The gate service tracker. */
	private ServiceTracker<BundleContext, Object> gateServiceTracker;
	
	/** The bacnet gate ref. */
	private ServiceReference<?> bacnetGateRef;
	
	/** The dpws gate ref. */
	private ServiceReference<?> dpwsGateRef;
	

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	static BundleContext getContext()
	{
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception
	{
		log.info("Start Gateway-Bundle");
		
		Activator.context = bundleContext;
		
		log.info("Start Service-Tracker for new Gate-Interfaces");
		gateServiceTracker = new GateServiceTracker(context);
		gateServiceTracker.open();
		
		gatewayReg = context.registerService(GatewayInterface.class.getName(), new GatewayImpl(), null);
		gatewayService = (GatewayImpl) context.getService(gatewayReg.getReference());
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception
	{
		log.info("Stop Gateway-Bundle");
		
		gateServiceTracker.close();
		log.info("Service-Tracker close");
		
		if ( bacnetGateRef != null)
			context.ungetService(bacnetGateRef);
		
		if ( dpwsGateRef != null)
			context.ungetService(dpwsGateRef);
		
		gatewayReg.unregister();
		
		Activator.context = null;
	}
	
	
	/**
	 * The Class GateServiceTracker.
	 */
	class GateServiceTracker extends ServiceTracker<BundleContext, Object>
	{
		
		/**
		 * Instantiates a new gate service tracker.
		 *
		 * @param context the context
		 */
		public GateServiceTracker(BundleContext context)
		{
			super (context, GateInterface.class.getName(), null);
		}
		
		/* (non-Javadoc)
		 * @see org.osgi.util.tracker.ServiceTracker#addingService(org.osgi.framework.ServiceReference)
		 */
		@Override
		public Object addingService(ServiceReference reference)
		{
			GateInterface gate = (GateInterface) context.getService(reference);
			
			if (gate.getClass().getName() == "org.unirostock.bagateway.bacnet.BACnetGateImpl")
			{
				log.info("New BACnet-Gate added!");
				
				bacnetGateRef = reference;
				gatewayService.setBACnetGate(gate);
				gate.openGate();
				//gate.discoverDevice(null);
				
			} else if (gate.getClass().getName() == "org.unirostock.bagateway.dpws.DPWSGateImpl")
			{
				log.info("New DPWS-Gate added!");
				
				dpwsGateRef = reference;
				gatewayService.setDPWSGate(gate);
				gate.openGate();
			} else {
				log.info("New unknown Gate added!" + gate.getClass().getName());
				return null;
			}
			
			return gate;
		}
		
		/* (non-Javadoc)
		 * @see org.osgi.util.tracker.ServiceTracker#removedService(org.osgi.framework.ServiceReference, java.lang.Object)
		 */
		@Override
		public void removedService(ServiceReference reference, Object gate)
		{
			if (gate.getClass().getName() == "org.unirostock.bacnetdpws.bacnetgate.BACnetGateImpl")
			{
				log.info("Gate-Interface removed! " + gate.getClass().getName());
				gatewayService.setBACnetGate(null);
				context.ungetService(reference);
				
			} else if (gate.getClass().getName() == "org.unirostock.bacnetdpws.dpwsgate.DPWSGateImpl")
			{
				log.info("Gate-Interface removed! " + gate.getClass().getName());
				gatewayService.setDPWSGate(null);
				context.ungetService(reference);
			}
		}
	}
}
