/*
 * File: GatewayImpl.java
 * Project: org.uniRostock.bacnetDPWS.gateway
 * @author Robert Balla
 */
package org.unirostock.bagateway;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.unirostock.bagateway.entitiy.GatewayInternalEntity;
import org.unirostock.bagateway.entitiy.ParameterEntity;
import org.unirostock.bagateway.entitiy.RootEntity;
import org.unirostock.bagateway.entitiy.SubEntity;
import org.unirostock.bagateway.interfaces.GateInterface;
import org.unirostock.bagateway.interfaces.GatewayInterface;

/**
 * The Class GatewayImpl.
 */
public class GatewayImpl implements GatewayInterface
{
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(GatewayImpl.class.getName());
	
	/** The bacnet gate. */
	private GateInterface bacnetGate;
	
	/** The dpws gate. */
	private GateInterface dpwsGate;
	
	/** The entity list. */
	private Hashtable<String, RootEntity> entityList; 
	
	/**
	 * Instantiates a new gateway impl.
	 */
	public GatewayImpl()
	{
		this.makeLogFile();
		
		bacnetGate = null;
		dpwsGate = null;
		
		entityList = new Hashtable<String, RootEntity>();
	}
	
	/**
	 * Sets the BACnet gate.
	 *
	 * @param gate the new BA cnet gate
	 */
	public void setBACnetGate(GateInterface gate)
	{
		bacnetGate = gate;
	}
	
	/**
	 * Gets the BA cnet gate.
	 *
	 * @return the BA cnet gate
	 */
	public GateInterface getBACnetGate()
	{
		return bacnetGate;
	}
	
	/**
	 * Sets the DPWS gate.
	 *
	 * @param gate the new DPWS gate
	 */
	public void setDPWSGate(GateInterface gate)
	{
		dpwsGate = gate;
	}
	
	/**
	 * Gets the DPWS gate.
	 *
	 * @return the DPWS gate
	 */
	public GateInterface getDPWSGate()
	{
		return dpwsGate;
	}
	
	/* (non-Javadoc)
	 * @see org.unirostock.bagateway.interfaces.GatewayInterface#newDevice(java.lang.String, org.unirostock.bagateway.entitiy.RootEntity)
	 */
	@Override
	public void newDevice(String protocol, RootEntity ent)
	{
		/*
		 * Check if the entity (device) is already known
		 */
		if (!entityList.contains(ent))
		{
			/* 
			 * forward the new entity (device) to the appropriate gate
			 * to announce it
			 * and get it back with new entry in identifierTable
			 */
			entityList.put(ent.getPath().getFirst().toString(), ent);
			
			if (protocol.equals("BACnet"))
			{
				if (dpwsGate != null)
					ent = dpwsGate.newDevice(ent);
			}
			else if (protocol.equals("DPWS"))
			{
				if (bacnetGate != null)
					ent = bacnetGate.newDevice(ent);
			}
		}
		else
		{
			/* 
			 * forward the known entity (device) to the appropriate gate
			 * to announce it
			 */
			if (protocol.equals("BACnet"))
			{
				if (dpwsGate != null)
					dpwsGate.announceDevice(entityList.get(ent.getPath().getFirst().toString()));
			}
			else if (protocol.equals("DPWS"))
			{
				if (bacnetGate != null)
					bacnetGate.announceDevice(entityList.get(ent.getPath().getFirst().toString()));
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.unirostock.bacnetdpws.GatewayInterface#discoverDev(java.lang.String, java.lang.String)
	 */
	@Override
	public void discoverDevice(String protocol, String name)
	{
		/*
		 * forward the discovery-request to the appropriate gate 
		 */
		if (protocol.equals("BACnet"))
		{
			if(dpwsGate != null)
			{
				dpwsGate.discoverDevice(name);
			}
			else
			{
				log.info("No DPWS-Gate found!");
			}
		}
		else if (protocol.equals("DPWS"))
		{
			if(bacnetGate != null)
			{
				bacnetGate.discoverDevice(name);
			}
			else
			{
				log.info("No BACnet-Gate found!");
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.unirostock.bagateway.interfaces.GatewayInterface#readValue(org.unirostock.bagateway.entitiy.GatewayInternalEntity)
	 */
	@Override
	public Hashtable<Object, String> readValue(LinkedList<Object> path, Hashtable<Object, String> request)
	{
		/*
		 * get the rootEntity of the targetEntity
		 */
		RootEntity rootEnt = entityList.get(path.getFirst().toString());
		
		/*
		 * forward the read-request to the gate the targetEntity belongs to
		 * and get the value
		 */
		if (rootEnt.getOriginProtocol().equals("BACnet"))
		{
			return bacnetGate.readValue(rootEnt, path, request);
		}
		else if (rootEnt.getOriginProtocol().equals("DPWS"))
		{
			return dpwsGate.readValue(rootEnt, path, request);
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see org.unirostock.bagateway.interfaces.GatewayInterface#writeValue(org.unirostock.bagateway.entitiy.GatewayInternalEntity, java.lang.String)
	 */
	@Override
	public void writeValue(LinkedList<Object> path, Hashtable<Object, String> request)
	{
		/*
		 * get the rootEntity of the targetEntity
		 */
		RootEntity rootEnt = entityList.get(path.getFirst().toString());
		
		/*
		 * forward the read-request to the gate the targetentity belongs to
		 */
		if (rootEnt.getOriginProtocol().equals("BACnet"))
		{
			bacnetGate.writeValue(rootEnt, path, request);
		}
		else if (rootEnt.getOriginProtocol().equals("DPWS"))
		{
			dpwsGate.writeValue(rootEnt, path, request);
		}
	}
	
	@Override
	public void subscribe(LinkedList<Object> path, long duration)
	{
		/*
		 * get the rootEntity of the targetEntity
		 */
		RootEntity rootEnt = entityList.get(path.getFirst().toString());
		
		/*
		 * forward the read-request to the gate the targetentity belongs to
		 */
		if (rootEnt.getOriginProtocol().equals("BACnet"))
		{
			bacnetGate.subscribe(rootEnt, path, duration);
		}
		else if (rootEnt.getOriginProtocol().equals("DPWS"))
		{
			dpwsGate.subscribe(rootEnt, path, duration);
		}
	}

	@Override
	public void notification(LinkedList<Object> path,
			Hashtable<Object, String> value)
	{
		/*
		 * get the rootEntity of the targetEntity
		 */
		RootEntity rootEnt = entityList.get(path.getFirst().toString());
		
		/*
		 * forward the read-request to the gate the targetentity belongs to
		 */
		if (rootEnt.getOriginProtocol().equals("BACnet"))
		{
			dpwsGate.notification(rootEnt, path, value);
		}
		else if (rootEnt.getOriginProtocol().equals("DPWS"))
		{
			bacnetGate.notification(rootEnt, path, value);
		}
		
	}

	@Override
	public void setLastValueOfParameter(LinkedList<Object>  entPath, Object param, String value)
	{
		Iterator<Object> pathIt = entPath.iterator();
		
		/*
		 * get the rootEntity of the targetEntity
		 */
		GatewayInternalEntity ent = entityList.get(pathIt.next().toString());
		
		while (pathIt.hasNext())
		{
			ent = ent.getSubEntity(pathIt.next());
		}
		
		((SubEntity) ent).getParameter(param).setLastValue(value);
			
	}

	@Override
	public String getLastValueOfParameter(LinkedList<Object> entPath, Object param)
	{
		Iterator<Object> pathIt = entPath.iterator();
		
		/*
		 * get the rootEntity of the targetEntity
		 */
		GatewayInternalEntity ent = entityList.get(pathIt.next().toString());
		
		while (pathIt.hasNext())
		{
			ent = ent.getSubEntity(pathIt.next());
		}
		
		ParameterEntity p = ((SubEntity) ent).getParameter(param);
		
		return p.getLastValue();
	}
	
	private void makeLogFile()
	{
		FileHandler fh; 
		
		try {  
	        fh = new FileHandler("DPWSGate.log");  
	        log.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  

	    } catch (SecurityException | IOException e) {  
	        e.printStackTrace();  
	    }  
	}
}
