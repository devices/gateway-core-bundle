/*
 * File: GateInterface.java
 * Project: org.unirostock.bagateway
 * @author Robert Balla
 */

package org.unirostock.bagateway.interfaces;

import java.util.Hashtable;
import java.util.LinkedList;

import org.unirostock.bagateway.entitiy.RootEntity;

// TODO: Auto-generated Javadoc
/**
 * The Interface GateInterface.
 */
public interface GateInterface
{
	/**
	 * Open gate.
	 */
	public void openGate();

	/**
	 * Close gate.
	 */
	public void closeGate();
	
	/**
	 * New device.
	 *
	 * @param dev the device
	 */
	public RootEntity newDevice(RootEntity ent);
	
	/**
	 * Announce device.
	 *
	 * @param dev the device
	 */
	public void announceDevice(RootEntity ent);
	
	/**
	 * Discover dev.
	 *
	 * @param dev the dev
	 */
	public void discoverDevice(String name);
	
	public Hashtable<Object, String> readValue(RootEntity rootEnt, LinkedList<Object> path, Hashtable<Object, String> request);
	public void   writeValue(RootEntity rootEnt, LinkedList<Object> path, Hashtable<Object, String> request);
	public void subscribe(RootEntity rootEnt, LinkedList<Object> path, long duration);
	public void notification(RootEntity rootEnt, LinkedList<Object> path, Hashtable<Object, String> value);
}
