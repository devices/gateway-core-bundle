/*
 * File: GatewayInterface.java
 * Project: org.unirostock.bagateway
 * @author Robert Balla
 */

package org.unirostock.bagateway.interfaces;

import java.util.Hashtable;
import java.util.LinkedList;

import org.unirostock.bagateway.entitiy.RootEntity;

// TODO: Auto-generated Javadoc
/**
 * The Interface GatewayInterface.
 */
public interface GatewayInterface
{
	
	/**
	 * New device.
	 *
	 * @param protocol the protocol
	 * @param dev the dev
	 * @return the root entity
	 */
	public void newDevice(String protocol, RootEntity ent);
	
	/**
	 * Discover device.
	 *
	 * @param protocol the protocol
	 * @param deviceID the device id
	 */
	public void discoverDevice(String protocol, String name);
	
	public Hashtable<Object, String> readValue(LinkedList<Object> path, Hashtable<Object, String> request);
	public void   writeValue(LinkedList<Object> path, Hashtable<Object, String> request);
	public void subscribe(LinkedList<Object> path, long duration);
	public void notification(LinkedList<Object> path, Hashtable<Object, String> value);
	
	public void setLastValueOfParameter(LinkedList<Object> entPath, Object param, String value);
	public String getLastValueOfParameter(LinkedList<Object> path, Object param);
}
